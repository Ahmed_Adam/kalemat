//
//  UserProfile.swift
//  Kalemat
//
//  Created by mac on 5/21/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation


class UserProfile: Codable {
    
    var address : String!
    var apiToken : String!
    var city : String!
    var countryId : Int!
    var mobile : String!
    var titleId : Int!
    var userFullName : String!
    
    
}

extension UserProfile {
    enum CodingKeys: String, CodingKey {
        case address  = "address"
        case apiToken  = "api_token"
        case city  = "city"
        case countryId  = "country_id"
        case mobile  = "mobile"
        case titleId  = "title_id"
        case userFullName  = "user_full_name"
        
        
    }
}
