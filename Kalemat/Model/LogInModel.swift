//
//  LogInModel.swift
//  Kalemat
//
//  Created by mac on 5/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation

struct LogIn: Codable {
    
    var check: Bool!
    var code: Int!
    var message: String!
    var data:  [UserData]!
    

    
}
extension LogIn {
    enum CodingKeys: String, CodingKey {
        case check
        case code
        case message
        case data

    }
}
