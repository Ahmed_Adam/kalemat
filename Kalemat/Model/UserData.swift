//
//  UserData.swift
//  Kalemat
//
//  Created by mac on 5/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation

struct UserData: Codable {

    var address : String!
    var apiToken : String!
    var check : Bool!
    var city : String!
    var code : Int!
    var countryId : Int!
    var mobile : String!
    var titleId : Int!
    var userFullName : String!

    var message: String!

    
}
extension UserData {
    enum CodingKeys: String, CodingKey {
        case address = "address"
        case apiToken = "api_token"
        case check
        case city
        case code
        case countryId = "country_id"
        case mobile = "mobile"
        case titleId =  "title_id"
        case userFullName = "user_full_name"
        
        case message = "msg"
    }
}
