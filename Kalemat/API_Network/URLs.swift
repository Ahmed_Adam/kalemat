//
//  URLs.swift
//  Kalemat
//
//  Created by mac on 5/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
struct URLs {
    
    //******** Login url ******** //
    static let login_en =   "http://kalemat.easydevs.com/mobile/v1/en/login"
    static let login_ar =   "http://kalemat.easydevs.com/mobile/v1/ar/login"
    
    //******** register url ******** //
    static let register_en = "http://kalemat.easydevs.com/mobile/v1/en/register"
    static let register_ar = "http://kalemat.easydevs.com/mobile/v1/ar/register"
    
    //******** activate_user ******** //
    static let activate_user_en = "http://kalemat.easydevs.com/mobile/v1/en/activate_user/"
    static let activate_user_ar = "http://kalemat.easydevs.com/mobile/v1/ar/activate_user/"
    
    //******** resend_activation_code ******** //
    static let resend_activation_code_en = "http://kalemat.easydevs.com/mobile/v1/en/resend_activation_code/"
    static let resend_activation_code_ar = "http://kalemat.easydevs.com/mobile/v1/ar/resend_activation_code/"
    
    //******** forget_password ******** //
    static let forget_password_en = "http://kalemat.easydevs.com/mobile/v1/en/forget_password/"
    static let forget_password_ar = "http://kalemat.easydevs.com/mobile/v1/ar/forget_password/"
    
    //******** change_forget_password ******** //
    static let change_forget_password_en = "http://kalemat.easydevs.com/mobile/v1/en/change_forget_password/"
    static let change_forget_password_ar = "http://kalemat.easydevs.com/mobile/v1/ar/change_forget_password/"
    
    //******** change_password ******** //
    static let change_password_en = "http://kalemat.easydevs.com/mobile/v1/en/change_password/"
    static let change_password_ar = "http://kalemat.easydevs.com/mobile/v1/ar/change_password/"
    
    //******** get_user_profile ******** //
    static let get_user_profile_en = "http://kalemat.easydevs.com/mobile/v1/en/get_user_profile"
    static let get_user_profile_ar = "http://kalemat.easydevs.com/mobile/v1/ar/get_user_profile"
    
    //******** update_user_profile ******** //
    static let update_user_profile_en = "http://kalemat.easydevs.com/mobile/v1/en/update_user_profile/"
    static let update_user_profile_ar = "http://kalemat.easydevs.com/mobile/v1/ar/update_user_profile/"
    
    //******** add_rate/******** //
    static let add_rate_en = "http://kalemat.easydevs.com/mobile/v1/en/add_rate/"
    static let add_rate_ar = "http://kalemat.easydevs.com/mobile/v1/ar/add_rate/"
    
    //******** get_aboutus ******** //
    static let get_aboutus_en = "http://kalemat.easydevs.com/mobile/v1/en/get_aboutus"
    static let get_aboutus_ar = "http://kalemat.easydevs.com/mobile/v1/ar/get_aboutus"
    
    //******** get_countrys ******** //
    static let get_countrys_en = "http://kalemat.easydevs.com/mobile/v1/en/get_countrys/"
    static let get_countrys_ar = "http://kalemat.easydevs.com/mobile/v1/ar/get_countrys/"
    
    //******** get_country_data ******** //
    static let get_country_data_en = "http://kalemat.easydevs.com/mobile/v1/en/get_country_data/"
    static let get_country_data_ar = "http://kalemat.easydevs.com/mobile/v1/ar/get_country_data/"
    
     
}
