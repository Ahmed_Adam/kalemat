//
//  UserAPIs.swift
//  Kalemat
//
//  Created by mac on 5/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class UserAPIs{
    
    static let sharedInstance = UserAPIs()
    
    
    //******** Login Request ******** //
    
    
    func login (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ code:Int , _ LogInData : UserData) -> Void){
        
        let url = URLs.login_en
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseData { (response) in
            
            switch response.result {
                
            case .success( let data ):

               let jsonString = data
               //***** Data Returned *****//
                let dataFromJson = try? JSONDecoder().decode(UserData.self, from: jsonString)
               let code = dataFromJson?.code
               let token = dataFromJson?.apiToken
               if token != nil {
               Token.saveAPIToken(token: token!)
               }
               complition(nil , true , code! , dataFromJson!)
               
                
            case .failure (let error):
                print(error)
                
                
            }
        }
    }
    
   // **************************************************************** //
    
    
    //******** Register Request ******** //
    
    
    func register (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ regsterModel:RegisterModel ) -> Void){
        
        let url = URLs.register_en
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseData { (response) in
            
            switch response.result {
                
            case .success( let data ):
                
                let jsonString = data
                //***** Data Returned *****//
                let dataFromJson = try? JSONDecoder().decode(RegisterModel.self, from: jsonString)

                complition(nil , true , dataFromJson! )
                
                
            case .failure (let error):
                print(error)
                
                
            }
        }
    }
    
    // **************************************************************** //
    
    
    //******** ActivationCode Request ******** //
    
    
    func ActivationCode (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ code:Int , _ acivationcode : ActivationCodeModel) -> Void){
        
        let url = URLs.activate_user_ar
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseData { (response) in
            
            switch response.result {
                
            case .success( let data ):
                
                let jsonString = data
                //***** Data Returned *****//
                let dataFromJson = try? JSONDecoder().decode(ActivationCodeModel.self, from: jsonString)
                let code = dataFromJson?.code
                complition(nil , true , code! , dataFromJson!)
                
                
            case .failure (let error):
                print(error)
                
                
            }
        }
    }
    
    // **************************************************************** //
    
    //********  Resend Activation code  Request ******** //
    
    
    func ReActivationCode (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ code:Int , _ acivationcode : ActivationCodeModel) -> Void){
        
        let url = URLs.resend_activation_code_en
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseData { (response) in
            
            switch response.result {
                
            case .success( let data ):
                
                let jsonString = data
                //***** Data Returned *****//
                let dataFromJson = try? JSONDecoder().decode(ActivationCodeModel.self, from: jsonString)
                let code = dataFromJson?.code
                complition(nil , true , code! , dataFromJson!)
                
                
            case .failure (let error):
                print(error)
                
                
            }
        }
    }
    
    // **************************************************************** //
    
    
    //********  Resend  Request ******** //
    
    
    func ForgetPassword  (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ code:Int , _ forgetPassword : RegisterModel) -> Void){
        
        let url = URLs.forget_password_en
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseData { (response) in
            
            switch response.result {
                
            case .success( let data ):
                
                let jsonString = data
                //***** Data Returned *****//
                let dataFromJson = try? JSONDecoder().decode(RegisterModel.self, from: jsonString)
                let code = dataFromJson?.code
                complition(nil , true , code! , dataFromJson!)
                
                
            case .failure (let error):
                print(error)
                
                
            }
        }
    }
    
    // **************************************************************** //
    
    
    //******** GetUserProfile ******** //
    
    
    func GetUserProfile ( complition :   @escaping (_ error:Error? ,_ success: Bool  , _ userProfile : UserProfile) -> Void){
        
        let token = Token.getAPIToken()
        let url = URLs.get_user_profile_en + "?api_token=" + "\(token!)"
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
         //***** Alamofire request *****//
        Alamofire.request(request).responseData  { (response) in
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success( let data ):
                
                let jsonString = data
                //***** Data Returned *****//
                let dataFromJson = try? JSONDecoder().decode(UserProfile.self, from: jsonString)
                
                complition(nil , true  , dataFromJson!)

                
                
            }
        }
    }
    
    // **************************************************************** //
    
    //******** GetCountry ******** //
    
    
    func GetCountry ( complition :   @escaping (_ error:Error? ,_ success: Bool  , _ userProfile : [CountryModel]) -> Void){
        
        
        let url = URLs.get_countrys_en
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseData  { (response) in
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success( let data ):
                
                let jsonString = data
                //***** Data Returned *****//
                let dataFromJson = try? JSONDecoder().decode([CountryModel].self, from: jsonString)
                
                complition(nil , true  , dataFromJson!)
                
                
                
            }
        }
    }
    
    // **************************************************************** //
    
}
