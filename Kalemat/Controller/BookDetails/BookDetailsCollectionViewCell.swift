//
//  BookDetailsCollectionViewCell.swift
//  Kalemat
//
//  Created by mac on 5/7/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class BookDetailsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var logobook: UIImageView!
    @IBOutlet weak var bookView: UIView!
    override func awakeFromNib() {
        bookView.layer.borderWidth = 1
        
        bookView.layer.borderColor = UIColor.brown.cgColor
    }

}
