//
//  BookDetailsViewController.swift
//  Kalemat
//
//  Created by mac on 5/7/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class BookDetailsViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UITextFieldDelegate{


    override func viewDidLoad() {
        super.viewDidLoad()
        BookCollectionView.delegate = self
        BookCollectionView.dataSource = self
        addToFavor.layer.borderWidth = 2
        addToFavor.layer.borderColor = UIColor.brown.cgColor
       
    }
    
    
    @IBOutlet weak var addToFavor: UIButton!
    
    
    @IBOutlet weak var BookCollectionView: UICollectionView!
      let logos = [#imageLiteral(resourceName: "book-5") , #imageLiteral(resourceName: "book-3") , #imageLiteral(resourceName: "1-book") , #imageLiteral(resourceName: "2-book") ,#imageLiteral(resourceName: "book-4")   ]
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = BookCollectionView.dequeueReusableCell(withReuseIdentifier: "bookDetailsCell", for: indexPath) as! BookDetailsCollectionViewCell
//        
//        let arrayOf_data = logos[indexPath.row]
//        cell.logobook.image = arrayOf_data
//        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "BookDeyails", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "BookDetailsViewController")
        self.show(linkingVC, sender: self)
        
    }
    
    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
        self.present(linkingVC, animated: true, completion: nil)
        
    }
    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

}
