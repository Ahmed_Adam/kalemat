//
//  CartProductCellTableViewCell.swift
//  Kalemat
//
//  Created by mac on 5/10/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class CartProductCellTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var item: UILabel!
    @IBOutlet weak var productCost: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var inreamentButton: UIButton!
    @IBOutlet weak var decrementButton: UIButton!
    @IBOutlet weak var totalPrice: UILabel!
    

}
