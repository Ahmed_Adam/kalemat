//
//  catCellTableViewCell.swift
//  Kalemat
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class catCellTableViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var backGround: UIImageView!
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var catName: UILabel!
    
}
