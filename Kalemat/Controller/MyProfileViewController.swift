//
//  MyProfileViewController.swift
//  Kalemat
//
//  Created by mac on 5/22/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import RSSelectionMenu


class MyProfileViewController: UIViewController , UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        profile.layer.borderWidth = 2
        profile.layer.borderColor = UIColor.brown.cgColor
        countryChoiceButon.layer.borderWidth = 1
        countryChoiceButon.layer.borderColor = UIColor.lightGray.cgColor
        cityChoiceButon.layer.borderWidth = 1
        cityChoiceButon.layer.borderColor = UIColor.lightGray.cgColor
        
//        userNameTextField.isEnabled = false
//        mobileTextField.isEnabled = false
//        AddressTextField.isEnabled = false
//        countryChoiceButon.isEnabled = false
//        cityChoiceButon.isEnabled = false
        
        getUserInfo()
        Getountries()
    }
    
     var countryNames = [String]()
    @IBOutlet weak var countryChoiceButon: UIButton!
    @IBOutlet weak var cityChoiceButon: UIButton!
    
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var AddressTextField: UITextField!
    
    func getUserInfo(){
        
        UserAPIs.sharedInstance.GetUserProfile { (error, success, userProfile) in
            
            self.userNameTextField.text = userProfile.userFullName
            self.mobileTextField.text = userProfile.mobile
            self.AddressTextField.text = userProfile.address
        }
        
        
    }
    
    @IBAction func EditUserInfo(_ sender: UIButton) {
        
        userNameTextField.isEnabled = true
        mobileTextField.isEnabled = true
        AddressTextField.isEnabled = true
        countryChoiceButon.isEnabled = true
        cityChoiceButon.isEnabled = true
    }
    
    @IBAction func SelectCountry(_ sender: UIButton) {
        
            
            // Show menu with datasource array - PresentationStyle = Formsheet & SearchBar
            
            let selectionMenu = RSSelectionMenu(dataSource: countryNames) { (cell, object, indexPath) in
                cell.textLabel?.text = object
            }
            
            // show selected items
           selectionMenu.setSelectedItems(items: countryNames) { (text, selected, selectedItems) in
                sender.setTitle(selectedItems[0], for: .normal)
            }
            
            // show searchbar with placeholder text and barTintColor
            // Here you'll get search text - when user types in seachbar
            
            selectionMenu.showSearchBar(withPlaceHolder: "Search Player", tintColor: UIColor.white.withAlphaComponent(0.3)) { (searchText) -> ([String]) in
                
                // return filtered array based on any condition
                // here let's return array where firstname starts with specified search text
                
                return self.countryNames.filter({ $0.lowercased().hasPrefix(searchText.lowercased()) })
            }
            
            // show as formsheet
            selectionMenu.show(style: .Formsheet, from: self)
      
  
    }
    
    
    func Getountries(){
        
        UserAPIs.sharedInstance.GetCountry { (error, success , countries) in
            for c in countries {
                self.countryNames.append(c.name) 
            }
        }
        
    }
   func getCities(){
    
    }
    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func back(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "Container") as! UITabBarController
        viewController.selectedIndex = 4
        self.show(viewController, sender: self)
        
    }
    

}
