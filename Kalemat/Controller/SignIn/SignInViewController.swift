//
//  SignInViewController.swift
//  Kalemat
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import RSLoadingView


class SignInViewController: UIViewController , UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        NameView.layer.borderWidth = 1
        NameView.layer.borderColor = UIColor.brown.cgColor

        
        passwordView.layer.borderWidth = 1
        passwordView.layer.borderColor = UIColor.brown.cgColor
        

    }
    @IBOutlet weak var NameView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    var rsponseMessage:String!
    
  
    @IBAction func LogIn(_ sender: UIButton) {
        
        let parameters = [
            "email"  : self.emailTextField.text,
            "password"  : self.passwordTextField.text
        ]
        
        UserAPIs.sharedInstance.login(info: parameters as! [String : String]) { (error ,success ,  code ,  LogInData)  in
            
            self.rsponseMessage = LogInData.message
            if LogInData.code == 0 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
            self.show(linkingVC, sender: self)
            }
            else {
                let loadingView = RSLoadingView()
                loadingView.shouldTapToDismiss = true
                loadingView.show(on: self.passwordView)
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                     loadingView.hide()
                    }
                  self.alertMessage()
                 }
        }
    }
    
    
    @IBAction func ForgetPassword(_ sender: UIButton) {
        
      //  let text = self.rsponseMessage//"خطأ في البريد الاكتروني او كلمة المرور"
        let alert = UIAlertController(title: "استعادة كلمة المرور", message: "" , preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = " البريد الاكتروني"
            textField.textAlignment = .right
        }
        
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { action in
            switch action.style{
            case .default:
                let info = ["":""]
                UserAPIs.sharedInstance.ForgetPassword(info: info, complition: { (error, success, code, forgetPassword) in
                    
                    if forgetPassword.code == 0 {
                        return
                    }
                    else {
                        return
                    }
                })
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        
    }
    
        
        
        func alertMessage ()
        {
            let text = self.rsponseMessage//"خطأ في البريد الاكتروني او كلمة المرور"
            let alert = UIAlertController(title: "تنبيه", message: text , preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
        }
        
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
        self.show(linkingVC, sender: self)
    }
    
}
