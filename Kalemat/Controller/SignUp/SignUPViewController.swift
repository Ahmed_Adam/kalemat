//
//  SignUPViewController.swift
//  Kalemat
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import SCLAlertView
import RSLoadingView

class SignUPViewController: UIViewController , UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NameView.layer.borderWidth = 1
        NameView.layer.borderColor = UIColor.brown.cgColor
        
        emailView.layer.borderWidth = 1
        emailView.layer.borderColor = UIColor.brown.cgColor
        
        passwordView.layer.borderWidth = 1
        passwordView.layer.borderColor = UIColor.brown.cgColor
        
        repasswordView.layer.borderWidth = 1
        repasswordView.layer.borderColor = UIColor.brown.cgColor
        
        activationView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        createAcount.isEnabled = true
    }
    
    @IBOutlet weak var NameView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repasswordView: UIView!
    @IBOutlet weak var repasswordTextField: UITextField!
    @IBOutlet weak var createAcount: UIButton!
    
    @IBOutlet weak var activationView: UIView!
    
    
    var regesterResponse : RegisterModel!
    var activationResponse : ActivationCodeModel!
    let kInfoTitle = "Activate User"
    let kSubtitle = ""
    var activateCode :String!
    
    @IBAction func register(_ sender: UIButton) {
        
        if (self.repasswordTextField.text == self.passwordTextField.text){
            
       let  parameters = [
            "email"  : self.emailTextField.text,
            "password"  : self.passwordTextField.text ,
            "full_name" : self.nameTextField.text
            ]

        UserAPIs.sharedInstance.register(info: parameters as! [String : String]) { (error ,success ,  regesterResponse )  in
            self.regesterResponse = regesterResponse
            switch regesterResponse.code {
            case 0 :
                self.activationView.isHidden = false
                let loadingView = RSLoadingView()
                loadingView.shouldTapToDismiss = true
                loadingView.show(on: self.activationView)
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                    
                    loadingView.hide()
                }
            default:
                self.alertMessage()
                
            }
        }
       }
        else{
            let alert = UIAlertController(title: "تنبيه", message: "كلمة المرور غير متطابقة" , preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    self.activationView.isHidden = true
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
        }
        
    }

    @IBOutlet weak var activatecodeText: UITextField!
    
    @IBAction func activate(_ sender: UIButton) {
        
        createAcount.isEnabled = false
            let  parameters = [
                "email"  : self.emailTextField.text!,
                "activation_code"  : activatecodeText.text!
            ]
            UserAPIs.sharedInstance.ActivationCode(info: parameters , complition: { (error, success, code, activationResponse) in
                self.activationResponse = activationResponse
                switch activationResponse.code {
                case 0 :
                 //  SignInViewController
    
                        let storyboard = UIStoryboard(name: "SignIn", bundle: nil)
                        let linkingVC = storyboard.instantiateViewController(withIdentifier: "SignInViewController")
                        self.show(linkingVC, sender: self)
                    //
                    
                    self.activationView.isHidden = true
                    
                case 4 :
                    self.alertMessageForActivate ()
                    
                    
                default:
                    self.alertMessageForActivate ()
                    
                }
            })
        }
    
    @IBAction func ReSendActivationCode(_ sender: UIButton) {
        
        let  parameters = [
            "email"  : self.emailTextField.text!,
        ]
        UserAPIs.sharedInstance.ReActivationCode(info: parameters , complition: { (error, success, code, activationResponse) in
            self.activationResponse = activationResponse
            switch activationResponse.code {
                
            case 0:
                self.alertMessage()
            default :
                self.alertMessage()
            }
        })
    }
    
    
    
    
    
    
    @IBAction func disMiss(_ sender: Any) {
       self.activationView.isHidden = true
        
    }
    
    func alertMessage ()
    {
        let text = regesterResponse.message
        let alert = UIAlertController(title: "تنبيه", message: text , preferredStyle: UIAlertControllerStyle.alert)
        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
        
                            case .cancel:
                                print("cancel")
                                self.activationView.isHidden = true
        
                            case .destructive:
                                print("destructive")
        
        
                            }}))
    }
    
    func alertMessageForActivate ()
    {
        let text = self.activationResponse.msg
        let alert = UIAlertController(title: "تنبيه", message: text , preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
    }
    

    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func back(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
        self.show(linkingVC, sender: self)
    }

}
