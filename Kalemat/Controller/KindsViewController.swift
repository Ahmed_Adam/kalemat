//
//  KindsViewController.swift
//  Kalemat
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class KindsViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout ,  UITextFieldDelegate{


    override func viewDidLoad() {
        super.viewDidLoad()
        catCollectionView.delegate = self
        catCollectionView.dataSource = self

      
    }
    @IBOutlet weak var catCollectionView: UICollectionView!
    
    let catNames = ["كتب عربية" ,"كتب انجليزية" ,"انتيكات" , "قرطسية" , "العاب اطفال" ,"العاب مرئية"]
    
    let logos = [#imageLiteral(resourceName: "كتب-عربية"),#imageLiteral(resourceName: "كتب-انجليزية") ,#imageLiteral(resourceName: "انتيكات") ,#imageLiteral(resourceName: "قرطسية") , #imageLiteral(resourceName: "العاب-اطفال-1") ,#imageLiteral(resourceName: "العاب-مرئية-1")]
    let bg = [#imageLiteral(resourceName: "89dbd105814672707a62068"),#imageLiteral(resourceName: "كتب-انجليزية-1") ,#imageLiteral(resourceName: "انتيكات-1") ,#imageLiteral(resourceName: "قرطسية-1") , #imageLiteral(resourceName: "العاب-اطفال") ,#imageLiteral(resourceName: "العاب-مرئية")]
    
    func collectionView(_ catCollectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat((collectionView.frame.size.width / 2) ), height: CGFloat(211))
    }
    
    
    
    
    func collectionView(_ catCollectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = catCollectionView.dequeueReusableCell(withReuseIdentifier: "categoriesCell", for: indexPath) as! catCellTableViewCell
        cell.backGround.image = bg[indexPath.row]
        cell.logo.image = logos[indexPath.row]
        cell.catName.text = catNames[indexPath.row]
        

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath.row) == 0 {
            let storyboard = UIStoryboard(name: "Library", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Library")
            self.show(linkingVC, sender: self)
        }
        else if (indexPath.row) == 1 {
            let storyboard = UIStoryboard(name: "EnglishLibrary", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "EnglishLibrary")
            self.show(linkingVC, sender: self)
        }
        
        else if (indexPath.row) == 2 {
            let storyboard = UIStoryboard(name: "anteks", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "anteks")
            self.show(linkingVC, sender: self)
        }
        else if (indexPath.row) == 3 {
            let storyboard = UIStoryboard(name: "kartes", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "kartes")
            self.show(linkingVC, sender: self)
        }
        else if (indexPath.row) == 4 {
            let storyboard = UIStoryboard(name: "ChildrenGames", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "ChildrenGames")
            self.show(linkingVC, sender: self)
        }
        else if (indexPath.row) == 5 {
            let storyboard = UIStoryboard(name: "VideoGames", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "VideoGames")
            self.show(linkingVC, sender: self)
        }
        
    } //anteks

    

    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

}
