//
//  categoryBooksCollectionViewCell.swift
//  Kalemat
//
//  Created by mac on 5/6/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class categoryBooksCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var logoBook: UIImageView!
    @IBOutlet weak var bookView: UIView!
    override func awakeFromNib() {
        bookView.layer.borderWidth = 1
        
        bookView.layer.borderColor = UIColor.brown.cgColor
    }
}
