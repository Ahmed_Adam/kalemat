//
//  LibraryViewController.swift
//  Kalemat
//
//  Created by mac on 5/6/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import ImageSlideshow
import SnapKit


class LibraryViewController: UIViewController , UITableViewDataSource , UITableViewDelegate, UICollectionViewDataSource , UICollectionViewDelegate , UITextFieldDelegate {

    lazy var slideshow = ImageSlideshow()
    lazy var logoImage = UIImageView()

    @IBOutlet weak var Slider: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.slideshow.setImageInputs([
            ImageSource(image:#imageLiteral(resourceName: "5")),
            ImageSource(image:#imageLiteral(resourceName: "4")),
            ImageSource(image:#imageLiteral(resourceName: "3")),
            ImageSource(image:#imageLiteral(resourceName: "1")),
            ImageSource(image:#imageLiteral(resourceName: "2"))
            ])
        self.slideshow.contentScaleMode = .scaleAspectFill
        self.slideshow.slideshowInterval = 4
        self.slideshow.pageControlPosition = .hidden
        
           self.Slider.addSubview(self.slideshow)
        
        self.slideshow.snp.makeConstraints { (make) in
            make.edges.equalTo(self.Slider).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        
        LibraryBookasTableView.delegate = self
        LibraryBookasTableView.dataSource = self
      
    }
    
    

    @IBAction func back(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "Container") as! UITabBarController
        viewController.selectedIndex = 1
        self.show(viewController, sender: self)
        
    }
    
    let logos = [#imageLiteral(resourceName: "book-5") , #imageLiteral(resourceName: "book-3") , #imageLiteral(resourceName: "1-book") , #imageLiteral(resourceName: "2-book") ,#imageLiteral(resourceName: "book-4")   ]
    @IBOutlet weak var LibraryBookasTableView: UITableView!

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LibraryBookasTableView.dequeueReusableCell(withIdentifier:"libraryBooks", for: indexPath) as! LibrayBooksTableViewCell
        
        self.libraryCategoryBooksCollectionView = cell.libraryCategoryBooksCollectionView
        cell.libraryCategoryBooksCollectionView.delegate = self
        cell.libraryCategoryBooksCollectionView.dataSource = self
        
        return cell
    }
    
    // collection view
    
    var libraryCategoryBooksCollectionView:UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = libraryCategoryBooksCollectionView.dequeueReusableCell(withReuseIdentifier: "categoryBooks", for: indexPath) as! categoryBooksCollectionViewCell
        
//        let arrayOf_data = logos[indexPath.row]
//        cell.logoBook.image = arrayOf_data
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "BookDeyails", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "BookDetailsViewController")
        self.show(linkingVC, sender: self)
    }
    
    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
