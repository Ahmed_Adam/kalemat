//
//  AllBookCollectionViewCell.swift
//  Kalemat
//
//  Created by mac on 5/9/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class AllBookCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var bookView: UIView!
    override func awakeFromNib() {
        bookView.layer.borderWidth = 1
        
        bookView.layer.borderColor = UIColor.brown.cgColor
    }
    
}
