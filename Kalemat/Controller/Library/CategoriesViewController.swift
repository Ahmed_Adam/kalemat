//
//  CategoriesViewController.swift
//  Kalemat
//
//  Created by mac on 5/7/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import ImageSlideshow
import SnapKit

class CategoriesViewController: UIViewController , UITextFieldDelegate {


    @IBOutlet weak var xxxx: UIView!
    @IBOutlet weak var cccc: UIView!
    @IBOutlet weak var costview: UIView!
    @IBOutlet weak var tartebView: UIView!
    @IBOutlet weak var tasnifView: UIView!
    //    lazy var slideshow = ImageSlideshow()
    //    lazy var logoImage = UIImageView()
    //    @IBOutlet weak var Slider: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        //        self.slideshow.setImageInputs([
        //            ImageSource(image:#imageLiteral(resourceName: "5")),
        //            ImageSource(image:#imageLiteral(resourceName: "4")),
        //            ImageSource(image:#imageLiteral(resourceName: "3")),
        //            ImageSource(image:#imageLiteral(resourceName: "1")),
        //            ImageSource(image:#imageLiteral(resourceName: "2"))
        //            ])
        //        self.slideshow.contentScaleMode = .scaleAspectFill
        //        self.slideshow.slideshowInterval = 4
        //        self.slideshow.pageControlPosition = .hidden
        //
        //        self.Slider.addSubview(self.slideshow)
        //
        //        self.slideshow.snp.makeConstraints { (make) in
        //            make.edges.equalTo(self.Slider).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        //        }
        
        
    }
    
    
    @IBOutlet weak var adb_W: UIButton!
    @IBAction func adb43r(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            adb_W.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            adb_W.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
    }
    
    @IBOutlet weak var storiesCheck: UIButton!
    @IBAction func storiesrowayat(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            storiesCheck.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            storiesCheck.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)   
       }
    
    }
    
    @IBOutlet weak var selfDevelop: UIButton!
    @IBAction func selfDevelop(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            selfDevelop.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            selfDevelop.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
        
    }
    
    @IBOutlet weak var health: UIButton!
    @IBAction func health(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            health.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            health.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
        
    }
    
    @IBOutlet weak var socialSience: UIButton!
    @IBAction func socialSience(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            socialSience.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            socialSience.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
        
    }
    
    @IBOutlet weak var generalBook: UIButton!
    @IBAction func generalBook(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            generalBook.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            generalBook.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
        
    }
    
    @IBOutlet weak var historyBooks: UIButton!
    @IBAction func historyBooks(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            historyBooks.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            historyBooks.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
        
    }
    
    @IBOutlet weak var hobits: UIButton!
    @IBAction func hobits(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            hobits.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            hobits.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
        
    }
    
    @IBOutlet weak var bussinessManagement: UIButton!
    @IBAction func bussinessManagement(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            bussinessManagement.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            bussinessManagement.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
        
    }
    
    
    @IBOutlet weak var coastLow_to_high: UIButton!
    @IBAction func coastLow_to_high(_ sender: UIButton) {
            coastLow_to_high.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
             coastHigh_to_low.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
    }
    
    
    @IBOutlet weak var coastHigh_to_low: UIButton!
    @IBAction func coastHigh_to_low(_ sender: UIButton) {
            coastHigh_to_low.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
            coastLow_to_high.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
    }
    
    
    @IBOutlet weak var tsa3dy: UIButton!
    @IBAction func tsa3dy(_ sender: UIButton) {
        tsa3dy.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        tnasly.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
    }
    
    
    @IBOutlet weak var tnasly: UIButton!
    @IBAction func tnasly(_ sender: UIButton) {
        tnasly.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        tsa3dy.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
    }
    
    @IBOutlet weak var free: UIButton!
    @IBAction func free(_ sender: UIButton) {
        free.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        NotFree.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
    }
    
    
    @IBOutlet weak var NotFree: UIButton!
    @IBAction func NotFree(_ sender: UIButton) {
        NotFree.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        free.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
    }
    
    @IBAction func backHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
        self.show(linkingVC, sender: self)
        //        navigationController?.present(linkingVC, animated: true, completion: nil)
        
    }

    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
