//
//  LibrayBooksTableViewCell.swift
//  Kalemat
//
//  Created by mac on 5/6/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class LibrayBooksTableViewCell: UITableViewCell , UICollectionViewDataSource , UICollectionViewDelegate{

    override func awakeFromNib() {
        super.awakeFromNib()
        //libraryCategoryBooksCollectionView.delegate = self
        //libraryCategoryBooksCollectionView.dataSource = self
    }
    
    
    @IBOutlet weak var more: UILabel!
    @IBOutlet weak var bestSeller: UILabel!
    
    
    
    @IBOutlet weak var libraryCategoryBooksCollectionView: UICollectionView!
    let logos = [#imageLiteral(resourceName: "book-5") , #imageLiteral(resourceName: "book-3") , #imageLiteral(resourceName: "1-book") , #imageLiteral(resourceName: "2-book") ,#imageLiteral(resourceName: "book-4")   ]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = libraryCategoryBooksCollectionView.dequeueReusableCell(withReuseIdentifier: "categoryBooks", for: indexPath) as! categoryBooksCollectionViewCell
        
        let arrayOf_data = logos[indexPath.row]
        cell.logoBook.image = arrayOf_data
        
        return cell
    }
    
  

}
