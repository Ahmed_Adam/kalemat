//
//  BookTypeViewController.swift
//  Kalemat
//
//  Created by mac on 5/7/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class BookTypeViewController: UIViewController ,UICollectionViewDelegate , UICollectionViewDataSource , UITextFieldDelegate {

    //        bookButton.layer.borderWidth = 2
    //        bookButton.layer.borderColor = UIColor.brown.cgColor
    
    @IBOutlet weak var bookView: UIView!
    @IBOutlet weak var AllbooksCOlletionView: UICollectionView!
     let logos = [#imageLiteral(resourceName: "book-5") , #imageLiteral(resourceName: "book-3") , #imageLiteral(resourceName: "1-book") , #imageLiteral(resourceName: "2-book") ,#imageLiteral(resourceName: "book-4")   ]
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        AllbooksCOlletionView.dataSource = self
        AllbooksCOlletionView.delegate = self

    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 50
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = AllbooksCOlletionView.dequeueReusableCell(withReuseIdentifier: "AllBooks", for: indexPath) as! AllBookCollectionViewCell
//
//        let arrayOf_data = logos[indexPath.row]
//        cell.logoBook.image = arrayOf_data
        
        return cell
    }
    
    @IBAction func backHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
        self.show(linkingVC, sender: self)
        //        navigationController?.present(linkingVC, animated: true, completion: nil)
        
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
}
