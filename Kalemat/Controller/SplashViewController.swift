//
//  SplashViewController.swift
//  Kalemat
//
//  Created by mac on 5/16/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        englishButton.layer.borderWidth = 2
        englishButton.layer.borderColor = UIColor.brown.cgColor

    }

  @IBOutlet weak var englishButton: UIButton!


}
