//
//  MyFavoritesViewController.swift
//  Kalemat
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class MyFavoritesViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate{
 
    

    override func viewDidLoad() {
        super.viewDidLoad()
        myFavTableView.delegate = self
        myFavTableView.dataSource = self
        
    }

    @IBOutlet weak var myFavTableView: UITableView!
    func tableView(_ myFavTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ myFavTableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = myFavTableView.dequeueReusableCell(withIdentifier: "myFavCell", for: indexPath)
        
        return cell
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "Container") as! UITabBarController
        viewController.selectedIndex = 4
        self.show(viewController, sender: self)
        
    }
    
    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
}
