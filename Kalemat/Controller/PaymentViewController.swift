//
//  PaymentViewController.swift
//  Kalemat
//
//  Created by mac on 5/10/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var delvaryStatic: UIButton!
       @IBOutlet weak var dhl: UIButton!
    
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var visa: UIButton!
       @IBOutlet weak var payPal: UIButton!
       @IBOutlet weak var cash: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        name.layer.borderWidth = 1
//        name.layer.borderColor = UIColor.brown.cgColor
        delvaryStatic.layer.borderWidth = 1
        delvaryStatic.layer.borderColor = UIColor.brown.cgColor
        dhl.layer.borderWidth = 1
        dhl.layer.borderColor = UIColor.brown.cgColor
        
        visa.layer.borderWidth = 1
        visa.layer.borderColor = UIColor.brown.cgColor
        payPal.layer.borderWidth = 1
        payPal.layer.borderColor = UIColor.brown.cgColor
        
        cash.layer.borderWidth = 1
        cash.layer.borderColor = UIColor.brown.cgColor
    }
    
    @IBAction func visaClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            sender.backgroundColor = UIColor.brown
            cash.backgroundColor = UIColor.white
            cash.isSelected = false
            payPal.backgroundColor = UIColor.white
            payPal.isSelected = false
        }
        else {
           sender.backgroundColor = UIColor.white
        }
        
    }
    
    @IBAction func cashClicked(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.backgroundColor = UIColor.brown
            payPal.isSelected = false
            payPal.backgroundColor = UIColor.white
            visa.isSelected = false
            visa.backgroundColor = UIColor.white
        }
        else {
            sender.backgroundColor = UIColor.white
        }
    }
    
    @IBAction func payPalClicked(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            sender.backgroundColor = UIColor.brown
            visa.backgroundColor = UIColor.white
            visa.isSelected = false
            cash.backgroundColor = UIColor.white
            cash.isSelected = false
        }
        else {
            sender.backgroundColor = UIColor.white
           
        }
    }
    
    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }


    @IBAction func back(_ sender: UIButton) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "Container") as! UITabBarController
        viewController.selectedIndex = 3
        self.show(viewController, sender: self)
        
    }
    
}
