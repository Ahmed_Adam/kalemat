//
//  SettingViewController.swift
//  Kalemat
//
//  Created by mac on 5/8/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController , UITextFieldDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
        @IBOutlet weak var voiceKeys: UIButton!
    @IBAction func voiceKeys(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            voiceKeys.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            voiceKeys.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
    }

    @IBOutlet weak var activeState: UIButton!
    @IBAction func activeState(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            activeState.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            activeState.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
    }
    
    @IBOutlet weak var brows: UIButton!
    @IBAction func brows(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            brows.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            brows.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
    }
    
    
    
    @IBOutlet weak var alarmOK: UIButton!
    @IBAction func alarmOK(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            alarmOK.setImage(#imageLiteral(resourceName: "ok_filled"), for: .normal)
        }
        else {
            alarmOK.setImage(#imageLiteral(resourceName: "icons8-ok"), for: .normal)
        }
    }
    
    @IBOutlet weak var alarmNO: UIButton!
    @IBAction func alarmNO(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            alarmNO.setImage(#imageLiteral(resourceName: "icons8-cancel"), for: .normal)
        }
        else {
            alarmNO.setImage(#imageLiteral(resourceName: "icons8-cancel_filled"), for: .normal)
        }
    }
    
    
    
    

    @IBAction func back(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "Container") as! UITabBarController
        viewController.selectedIndex = 4
        self.show(viewController, sender: self)
        
        
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

}
