//
//  CartViewController.swift
//  Kalemat
//
//  Created by mac on 5/10/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class CartViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate {
    


    var numOfItems:Int!
    
    var items = 1.0

    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var totalproPrice: UILabel!
    
    
    @objc func increment(_ sender: UIButton) {
        
        
   //  let index = sender.tag
        items = items + 1
    //    cartList[index].qty = Int(items)
      //  subtotal.text = "0.0"
      //  totPrice = 0.0
        self.tableview.reloadData()
    }
    
    @objc func decrament(_ sender: UIButton) {
        
   // let index = sender.tag
        if ( items > 1) {
            items = items - 1
       //     cartList[index].qty = Int(items)
         //   subtotal.text = "0.0"
         //   totPrice = 0.0
            self.tableview.reloadData()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // number of items in cart
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableview.dequeueReusableCell(withIdentifier: "ProductInCart", for: indexPath) as? CartProductCellTableViewCell
        
 //       cell?.item.text = "\(1)"
     //   cell?.item.text = String(cartList[indexPath.row].qty)
        cell?.inreamentButton.tag = indexPath.row
        cell?.inreamentButton.superview?.tag = indexPath.section
        cell?.inreamentButton.addTarget(self, action: #selector(increment(_:)), for: .touchUpInside)
        cell?.decrementButton.tag = indexPath.row
        cell?.decrementButton.superview?.tag = indexPath.section
        cell?.decrementButton.addTarget(self, action: #selector(decrament(_:)), for: .touchUpInside)
        cell?.qty.text = "\(Int(items))"
        var doubleCost:Double!
        var doubleQty:Double!
        if let cost = cell!.productCost.text {
             doubleCost = Double(cost)
        }
        if let qty = cell!.qty.text {
             doubleQty = Double(qty)
        }
       
        
     //   let totalproductPrice = doubleQty  *  doubleCost
        
  //      cell?.totalPrice.text = "\(totalproductPrice)"
  //     self.totalPrice.text = "\(totalproductPrice)"
        return cell!
    }
    
    
    @IBOutlet weak var tableview: UITableView!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
  
    }
   
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

}
