//
//  AboutViewController.swift
//  Kalemat
//
//  Created by mac on 5/9/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController , UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        FBLoginButton.layer.borderWidth = 1
        FBLoginButton.layer.borderColor = UIColor.brown.cgColor
        
        TwitterLoginButton.layer.borderWidth = 1
        TwitterLoginButton.layer.borderColor = UIColor.brown.cgColor
        
        PhoneLoginButton.layer.borderWidth = 1
        PhoneLoginButton.layer.borderColor = UIColor.brown.cgColor
        
        //            UIColor(red:201.0, green:149.0, blue:42.0, alpha: 0.5).cgColor
    }

  


@IBOutlet weak var FBLoginButton: UIButton!
@IBOutlet weak var TwitterLoginButton: UIButton!
@IBOutlet weak var PhoneLoginButton: UIButton!
    
    @IBAction func back(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "Container") as! UITabBarController
        viewController.selectedIndex = 4
        self.show(viewController, sender: self)
        
    }

    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
