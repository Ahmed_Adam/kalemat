//
//  HomeViewController.swift
//  Kalemat
//
//  Created by mac on 5/6/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate {


    @IBOutlet weak var bookButton: UIButton!
     let logos = [#imageLiteral(resourceName: "book-5") , #imageLiteral(resourceName: "book-3") , #imageLiteral(resourceName: "1-book") , #imageLiteral(resourceName: "2-book") ,#imageLiteral(resourceName: "book-4")   ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        bookButton.layer.borderWidth = 2
//        bookButton.y = UIColor.brown.cgColor
        LibraryBookasTableView.delegate = self
        LibraryBookasTableView.dataSource = self
        
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Library", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Library")
        self.show(linkingVC, sender: self)
        
    }
    
    

    @IBOutlet weak var LibraryBookasTableView: UITableView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LibraryBookasTableView.dequeueReusableCell(withIdentifier:"categoryBooks", for: indexPath) as! LibrayBooksTableViewCell
        

        cell.libraryCategoryBooksCollectionView.delegate = self
        cell.libraryCategoryBooksCollectionView.dataSource = self
        
        libraryCategoryBooksCollectionView = cell.libraryCategoryBooksCollectionView
        
        return cell
    }
    
    // collection view
    
    var libraryCategoryBooksCollectionView:UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = libraryCategoryBooksCollectionView.dequeueReusableCell(withReuseIdentifier: "categoryType", for: indexPath) 
        
    //    let arrayOf_data = logos[indexPath.row]
    //    cell.logoBook.image = arrayOf_data
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "BookDeyails", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "BookDetailsViewController")
        self.show(linkingVC, sender: self)
    }
    
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }


}
